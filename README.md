A simple chatroom application that uses django's channels module for chatting via websockets.

## Features
- Join a chatroom by visiting a provided link. If chatroom doesn't exist on this url, it will be created automatically
- Realtime chat
- Youtube and image links are auto expanded

## Running locally

To run this app locally, you'll need Python, Postgres, and Redis. Then:

- Install requirements: `pip install -r requirements.txt`.
- Set right configuration in chat/settings.py
```python
DATABASES = {
    'default': dj_database_url.config(default="postgres://postgres:root@localhost/chatroom-channels", conn_max_age=500)
}
```
- Make sure redis is running on port 6379
- `python manage.py migrate`
- `python manage.py runserver`


# Reference : Django Channels Example by Jacob Kaplan Moss [![Build Status](https://travis-ci.org/jacobian/channels-example.svg?branch=master)](https://travis-ci.org/jacobian/channels-example)
