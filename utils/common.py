import re
# import logging
# import os
# import datetime
# import random
# import tempfile
# import pytz
# import requests


# def anchor_tags(text):
#     urls = re.compile(r"((https?):((//)|(\\\\))+[\w\d:#@%/;$()~_?\+-=\\\.&]*)", re.MULTILINE|re.UNICODE)
#     return urls.sub(r'<a href="\1" target="_blank">\1</a>', text)


def image_embed(text):
    urls = re.compile(r"(((https?):((//)|(\\\\))+[\w\d:#@%/;$()~_?\+-=\\\.&]*)\.(?:jpg|gif|png))")
    return urls.sub(r'\1<br><img src="\1" style="max-height:200px;max-width:200px">', text)


def youtube_embed(text):
    urls = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', text)
    youtube_regex = (
        r'(https?://)?(www\.)?'
        '(youtube|youtu|youtube-nocookie)\.(com|be)/'
        '(watch\?v=|embed/|v/|.+\?v=)?([^&=%\?]{11})')
    out = text
    for u in urls:
        m = re.match(youtube_regex, u)
        if(m and m.group(6)):
            out = text + '<br><iframe width="200" height="140" src="https://www.youtube.com/embed/' +m.group(6)+ '" frameborder="0"allowfullscreen></iframe>'
            break
    return out
