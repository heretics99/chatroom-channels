from __future__ import unicode_literals

from utils import common
from django.db import models
from django.utils import timezone

class Room(models.Model):
    name = models.TextField()
    label = models.SlugField(unique=True)
    timestamp = models.DateTimeField(default=timezone.now, db_index=True)

    def __unicode__(self):
        return self.label

class Message(models.Model):
    room = models.ForeignKey(Room, related_name='messages')
    handle = models.TextField()
    message = models.TextField()
    timestamp = models.DateTimeField(default=timezone.now, db_index=True)

    def __unicode__(self):
        return '[{timestamp}] {handle}: {message}'.format(**self.as_dict())

    @property
    def formatted_message(self, urls=True, youtube=True, images=True):
        formatted_msg = self.message
        # if(urls):
        #     formatted_msg = common.anchor_tags(formatted_msg)
        if(images):
            formatted_msg = common.image_embed(formatted_msg)
        if(youtube):
            formatted_msg = common.youtube_embed(formatted_msg)
        return formatted_msg

    @property
    def formatted_timestamp(self):
        return self.timestamp.strftime('%b %d %I:%M %p')

    def as_dict(self):
        return {'handle': self.handle, 'message': self.formatted_message, 'timestamp': self.formatted_timestamp}