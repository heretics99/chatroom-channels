$(function() {

    var ws_scheme = window.location.protocol == "https:" ? "wss" : "ws";
    var url = ws_scheme + '://' + window.location.host + "/chat" + window.location.pathname;
    var chatsock = new ReconnectingWebSocket(url);

    chatsock.onmessage = function(message) {

        var data = JSON.parse(message.data);
        var chat = $("#chat-messages-ui");
        var ele = $('<div class="sent-message-true"><span class="username ng-binding">'+data.handle+': </span>'+data.message+'<br><span style="font-size: 10px">'+data.timestamp+'</span></div><div class="clear"></div>');
        chat.append(ele)
    };

    $("#chatwidget").on("submit", function(event) {
        var message = {
            handle: $('#handle-username').val(),
            message: $('#chat-textbox').val()
        };
        chatsock.send(JSON.stringify(message));
        $("#chat-textbox").val('').focus();
        return false;
    });
});